## Citations attendues

## Notes
`Voir pour afficher plus de contexte pour la cible (autour de la fenetre glissante)`


## Servius
-- Bella per Emathios

&& et Gallica certus limes ab Ausoniis disterminat arva colonis

&& profugique a gente vetusta Gallorum Celtae miscentes nomen Hiberis

&& nec si te pectore vates accipio

&& non soliti lusere sales (variante mss risere)

&& {un peu loin...} pacem summa tenent

&& arboribus suus horror inest

&& iuuit sumpta ducem iuuit dimissa potestas

&& contentique auspice Bruto

&& et turbata perit disperis littera pennis

&& accipiunt sertas nardo florente coronas

&& portusque quietos ostendit Libyae Phrygio placuisse magistro

&& { loin mais bcp de mots vides) atque undae plus quam quod digerat aer

&& sic male deseruit

&& Troianam soli cui fas vidisse Minervam (variante mss cui fas soli ; Mineruem)

&& {mal découpé cause des mots vides} et se dilecta Tritonida dixit ab unda

&& {commence à numinis} non ille favore numinis ingenti superum protectus ab ira

&& in pugnam fregere rates

  -- translata uitat contingere limina planta (variante mss translata uetat / translataque uetat)

&& cultus gestare decoros uix nuribus rapuere mares

&& Herceas, monstrator ait, non respicis aras?

&& {manque le sed} sed conditor artis finxerit ista Tages

&& cornus tibi cura sinistri Lentule

&& quaerentem erumpere ventum credidit

&& Stygiasque canes in luce superna destituam

&& nec meus Eudoxi vincetur fastibus annus (variante mss uincitur fastibus annos)

&& {'aetas omnis in' pas catché} venit aetas omnis in unam congeriem miserumque premunt tot saecula pectus + nec tantum prodere vati quantum scire licet

-- {match un truc qu'a rien à voir} atque hominem toto sibi cedere iussit pectore (variante mss dicere / caedere)

&& ensiferi nimium fulget latus Orionis

&& et dubios cernit vanescere montes

&& hospes in externis audivit curia tectis

&& sic male deseruit

&& contentique auspice Bruto

&& vectoris patiens tumidum supernatat amnem

&& non tu laetis ululare triumphis (variante mss nam ; om. Tu)

&& nec se tellure cadaver paulatim per membra levat terraque repulsum est erectumque semel (variante mss leuatque / uolat)

&& saevitia est voluisse mori

&& postquam se lumine vero implevit (variante mss miscuit au lieu de impleuit)

&& et laetae iurantur aves bubone sinistro

-- donec subpositas propius despumet in herbas

-- aemeret paruo

  -- Vt quotiens aestus Zephyris eurisque repugnat, Huc abeunt fluctus, illuc mare (variante mss aestu zephyri eurusque / illic)

&& fractaeque tonitrua nubes

&& non illuc auro positi, nec ture sepulti perveniunt (variante mss illic)

-- {match pas ce qu'il faut j'ai l'impression} et medio feriere die (variante mss feriente diem / ferit ense die d’où Stephanus ferit axe diem)

&& {loin, ptit score car pas de match sur paruo/parvo} aere merent paruo

&& stat numquam facies (variante mss stant)

&& aequora fracta vadis abruptaque terra profundo

&& cui numine mixto Delphica Thebanae referunt trieterica Bacchae

nos Cato da veniam

Hecate pars ultima nostrae

&& {commence à nocte} vidit quanta sub nocte iaceret nostra dies

cuius vos estis superi

&& pectora tunc primum ferventi sanguine supplet

x2? && non in tartareo latitantem poscimus antro adsuetamque diu tenebris, modo luce fugata descendentem animam: primo pallentis hiatu haeret adhuc Orci

&& hinc Abatos, quam nostra vocat veneranda vetustas

sterili non quicquam frigore gignit

&& {match entre 'ululantque' & 'anguis'} latratus habet illa canum gemitusque luporum: quod stridunt ululantque ferae, quod sibilat anguis exprimit et planctus fractaeque tonitrua nubis: tot rerum vox una fuit

&& Stygiasque canes in luce superna destituam

&& livor edax tibi cuncta negat, gentesque subactas

&& regit idem spiritus artus orbe alio : longae, canitis si cognita, vitae

&& Gallorum captus spoliis et Caesaris auro

&& {commence à spargis} qui uiscera saeuo spargis nostra cani

illic postquam se lumine vero induit

&& {'et nobis' pas catché} et nobis dabis improba poenas et superis quos fingis ait

&& {finit à lapso / suite de la citation dans un autre match} Aethiopumque solum, quod non premeretur ab ulla signiferi regione poli, nisi poplite lapso ultima curvati procederet ungula tauri

atque ardens aere solo

&& {manque le exactmatch} cognita per multos docuit rudis incola patres

ubi nobile quondam nunc super Argos arat (variante mss erat)

&& quodque ausa volare ardea sublimis pinnae confisa natanti

fluvios videt ille cruoris (variante mss fluuius ; uidit ; cruores

occurrat Hiberis alter

Cannarum fuimus Trebiaeque iuventus

quas Aliae clades

&& {commence à iam} harpen alterius monstri iam caede rubentem

ante venena nocens

turrigero canos effundens vertice crines

&& et subito feriere die

hospes et Alcidae magni, Phole (variante mss Alcides)

Hesperios inter Sicoris non ultimus amnis

&& {finit à aer} fulminibus terrae propior succenditur aer, pacem summa tenent

miratoremque Catonis

&& mox Lelegum dextra pressum descendit aratrum

&& at postquam virtus incerta virorum perpetuam rupit defesso milite cratem singula continuis cesserunt ictibus arma

&& discussa fugit ab ara taurus

&& plus patria potuisse sua

&& hunc aut tortilibus vibrata falarica nervis obruat

&& telorum nimbo peritura et pondere ferri

&& ocior et caeli flammis et tigride feta (variante mss fera / facta)

&& {commence à populis} crinemque rotantes sanguinei populis ulularunt tristia Galli

et summis longe petit aequora remis

&& et amissum fratrem lugentibus offert

&& quantum pede prima relato constringit gyros acies

&& gladiosque suos conpressa timebat

&& non pondera rerum, nec momenta sumus

non eget ingestis, sed vulsis pectore telis

ipsamque vocatam quem petat e nobis Mortem tibi coge fateri

&& Apulus Adriacas exit Garganus in undas

&& {segmentation commence à insula} nunc claustrum pelagi cepit Pharon: insula quondam in medio stetit illa mari sub tempore vatis Proteos, at nunc Pellaeis est proxima muris

&& subsidere regnum Chalcidos Euboicae magna spe rapte parabas

&& timuit, ne quas effundere voces vellet et aeternam fletu corrumpere famam

&& Brundisium decimis iubet hanc attingere castris

urbi pater est, urbique maritus

felici non fausta loco tentoria ponens

&& per ferrum tanti securus vulneris exit

&& elatasque alte, quaecumque ad bella vocaret, promisere manus

&& an melius fient piratae, magne, coloni

&& Edonis Ogygio decurrit plena Lyaeo

&& {rate le exactmatch à cause d'un mot à droite en dehors des ""} hospes in externis audivit curia tectis

nulloque a vertice tellus altius intumuit propiorque accessit Olympo

&& claudit odoratae metuentis aera pennae

&& feta tepefacta sub alite saxa

surgere congesto non culta mapalia culmo

&& atque in transtra cadunt et remis pectora pulsant

## Diomede

:( 242 <quote> «uittata sacerdos» </quote>
:) 5 <quote>«ut superi uoluere, late» </quote>
:) 2 <quote>«sic funere primo /attonitae patuere domus, cum corpora nondum / conclamata iacent»</quote>.


## Cledonii

<quote>«Caesar in arma furens»</quote>
<quote>«cum iam tabe fluunt»</quote>
<quote>«puppe propinqua»</quote>
<quote> «qualiter expressum uentis per nubila fulmen» </quote>

## Eutyches 
<quote>«continuus multis subitarum tractus aquarum / artauit clausitque amnem»</quote>
<quote> «auribus incertum feralis strideat umbra» </quote>
<quote> «indixitque nefas» </quote>
<quote> «caesorum truncare cadauera regum» </quote>
<quote>«dum bella relegem, / extremum Scythici transcendam frigoris axem»</quote>
<quote> «excussit cristas galeis capulosque solutis / perfudit gladiis ereptaque pila liquauit» </quote>
<quote> «inquire in fata nefandi Caesaris» </quote>

### Phocas

«exul adhuc iacet umbra ducis» -> ligne 17

  100	25	3023	3051	exul adhuc iacet umbra ducis 	<match> <el>exul</el> <el>adhuc</el> <el>iacet</el> <el>umbra</el> <el>ducis</el></match>

«et natrix uiolator aquae» -> ligne 33

  100	15	6414	3425	natrix uiolator aquae 	<match> concolor exustis atque indiscretus harenis hammodytes spinaque uagi torquente cerastae et scytale sparsis etiamnunc sola pruinis exuuias positura suas et torrida dipsas et grauis in geminum uergens caput amphisbaena et <el>natrix</el> <el>uiolator</el> <el>aquae</el> iaculique uolucres et contentus iter cauda sulcare parias oraque distendens auidus fumantia prester , ossaque dissoluens cum corpore tabificus seps , sibilaque effundens cunctas terrentia pestes , ante uenena nocens , late sibi summouet omne vulgus et in uacua regnat basiliscus harena</match>

«finibus extremis Libyes ubi feruida tellus» -> ligne 19 (WTF) extremis lemmatisé différemment mais normalement match sur les tokens
                                              -> 57% ratio : problème de calcul

  57.1428571428571	20	7076	3385	« finibus extremis libyes ubi feruida tellus 	<match> <el>finibus</el> extremis <el>libyes</el> , ubi <el>feruida</el> <el>tellus</el> accipit oceanum demisso sole calentem , squalebant late phorcynidos arua medusae , non nemorum protecta coma , non mollia suco , sed dominae uultu conspectis aspera saxis</match>


«frenosque momordit» -> ligne 510 (dur parce que très court mais présence EN)

  66.6666666666667	10	7977	1985	« frenosque momordit 	<match> primus ab aequorea percussis cuspide saxis thessalicus sonipes , bellis feralibus omen , exiluit , primus chalybem <el>frenosque</el> <el>momordit</el> spumauitque nouis lapithae domitoris habenis</match>



«traxerunt in arte magica uertigine fili» -> ligne 41

  60	15	7988	2006	traxerunt in arte magica uertigine fili 	<match> quos non concordia mixti alligat ulla tori blandaeque potentia formae , <el>traxerunt</el> torti <el>magica</el> <el>uertigine</el> fili</match>



«ad saxum quotiens ingenti uerberis ictu» -> ligne 26

  100	18	8508	893	ad saxum quotiens ingenti uerberis 	<match> at <el>saxum</el> <el>quotiens</el> <el>ingentis</el> <el>uerberis</el> actu excutitur , qualis rupes , quam uertice montis abscidit impulsu uentorum adiuta uetustas , frangit cuncta ruens , nec tantum corpora pressa exanimat , totos cum sanguine dissipat artus</match>



«degener o populus» -> ligne 38

  100	15	8868	389	degener o populus 	<match> mille licet gladii mortis noua signa sequantur , <el>degener</el> <el>o</el> <el>populus</el> , uix saecula longa decorum sic meruisse uiris , nedum breue dedecus aeui et uitam , dum sulla redit</match>



«aut scidit et medias fecit sibi litora terras» -> ligne 10

  100	25	11097	714	aut scidit et medias fecit sibi litora terras 	<match> curio sicanias transcendere iussus in urbes , qua mare tellurem subitis aut obruit undis aut <el>scidit</el> et <el>medias</el> <el>fecit</el> sibi <el>litora</el> <el>terras</el></match>
