lucanus dicendo « nec meus eudoxi vincetur fastibus annus » . servitio enixae tvlimvs hoc.

lucanus « crinemque rotantes sanguinei populis ulularunt tristia galli » . ‘ cybebe ’ autem.

svis etiam suis : lucanus « et amissum fratrem lugentibus offert » . discrimina differentias . thymbre.

corporum ; lucanus « qui viscera saevo spargis nostra cani » . tunc enim animae locum.

nunc claustrum pelagi cepit pharon : insula quondam in medio stetit illa mari sub tempore vatis proteos , at nunc pellaeis est proxima muris » .

numerum , ut lucanus « non soliti lusere sales » . aliquando etiam ‘ urbanitatem.
genetivum usurparit lucanus , ut « cornus tibi cura sinistri , lentule » . sic cicero in arato .

ait Lucanus «regit idem spiritus artus orbe alio: longae, canitis si cognita, vitae».

Ut « hunc aut tortilibus vibrata falarica nervis obruat » vergilius vero ait turnum .

apud neotericos invenimus ; lucanus « livor edax tibi cuncta negat , gentesque subactas » . longaeva sacerdos sibyllam apollo .


romam , de quo lucanus « gallorum captus spoliis et caesaris auro » . fixit leges pretio atqve .
