# Observations
  ```
  Résultats fenetre 3 traités
    - Problème récurrent pour les citations longues : empan phrastique pas forcément adapté
      - implication algo -> ?
      - coupe citation en 2 et baisse le score du tout
    -
  ```

# Retour pour FB
  ```
Fait-on le choix de limiter à 1 segment source un même segment cible.


5	203	100,00	57	57	« inquire in fata nefandi caesaris » , 	inquire in fata nefandi caesaris , et patriae uenturos excute mores	inquiris , lucanus libro viiii « inquire in fata nefandi caesaris » , conquiro conquiris . consonante 
7	323	66,67	25	16,6675	in fata nefandi caesaris » , 	sit praeter gladios aliquod sub caesare fatum quid uelut ignaros , ad quae portenta paremur , spe trahis	lucanus libro viiii « inquire in fata nefandi caesaris » , conquiro conquiris . consonante


## CAVAJONI
`perl create-index-latin-tt-v2.pl cavajoni.txt`

------------------

  ```

    - ok
      - les 18 premieres citations extraites avec windowSize 3 sont bonnes

    - bruit
      - segment introduit par :
        -ordo verborum (1 seul cas :/)
      - paraphrase -> indices lexicaux ? typographiques ?
      - reformulation & hypallage -> pas d'indices ?


    - silence
      - "non tu Pyrre ferox"
      - "ventus ab extremo"
      - "non uitabat contingere limina planta"
      -  Pharios concute reges » (qui apparaît sous la forme ‘ pharios ’ , id est aegyptios , ‘ concute reges ’) --> chaud celle là. c'est comme si c'était une citation de 2... vu la distance avec le 3eme mot.
      - "cum premeret uiscera partus explicuit Pithona" 
      - "populus qui nunc est" --> trop de mots "vides" ?
        - voir avec FB pour liste de mots vides ?
      - "haec uult turba libera mori"
      - "in uentos expendo uota fretumque"

  ```
## SERVIUS
  `perl create-index-latin-tt-v2.pl servius.txt`

74 premiers résultats ordonnés sont bons.

1er élément de bruit dans les resultats.
5026	66,67	50	33,335	proxima ponto ’ , 	hoc tam segne solum raras tamen exerit herbas , quas nasamon , gens dura , legit , qui proxima ponto nudus rura tenet , quem mundi barbara damnis syrtis alit	nunc dicit ‘ portae quae proxima ponto ’ , item paulo post dicturus

## Performance

  - id de phrases pour la fenetre 3 du fichier cavaoni envoyé à FB
