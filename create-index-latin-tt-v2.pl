use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
# use warnings;
use utf8;
use Data::Dumper;
use experimental 'smartmatch';
use locale;
use Time::HiRes qw(time);
use Cwd qw(cwd);
use lib './lib';
use Term::ANSIColor;
use StringManager;
use ExportManager;
use IndexManager;
use ParameterManager;

our $start_run    = time();
our $progress     = 0;
our $oldProgress  = 0;
######################################### Parameters #########################################
our %params           = ParameterManager::getParams();
our ($cibleFile)      =  @ARGV;
my $cibleFilePath     =  "input/tt/".$cibleFile;
my $sourceFilePath    =  "input/tt/".$params{"sourceFile"};
my $prefixFileOut     = $cibleFile;
if($cibleFile=~/^(.+)\.[^\.]+$/){
  $prefixFileOut      = $1."_g-".$params{"gapFactor"} . "_w-".$params{"windowSize"};
}
######################################### Début Variables #######################################
our %hitsInSource;
our %oneHitPerSent;           # $oneHitPerSent{id_sent}{"89-98"}=$keyHit
our %hitsPerQuotePattern;     # $hitsPerQuotePattern{$cible}=$score
our %idsCibleToScore;
######################################### Fin Variables #########################################
open( LOG, ">:encoding(utf8)", "logs/log-".$cibleFile );
######################################### Routines ##############################################
our $calculScoreIteration = 0;
our $totalSentSourceRoutine;
our (
    $idToLineCible,           $idToLineSource,
    $idSourceToTokensToCount, $tokenToIdLineCible,
    $tokenToIdLineSource,     $lemmaCountToIdSourcePm,
    $lemmaToIdLineCible,      $lemmaToIdLineSource,
    $cibleLines,              $tokenCibleToFuzzySourcePm,
    $lemmaCibleToFuzzySource
  ) = IndexManager::launchAndGetVariables($cibleFile, \%params );

my %idToLineCible           = %{$idToLineCible};
my @cibleLines              = @{$cibleLines};
my %idToLineSource          = %{$idToLineSource};
my %idSourceToTokensToCount = %{$idSourceToTokensToCount};
my %tokenToIdLineCible      = %{$tokenToIdLineCible};
my %tokenToIdLineSource     = %{$tokenToIdLineSource};
my %lemmaCountToIdSource    = %{$lemmaCountToIdSourcePm};
my %lemmaToIdLineCible      = %{$lemmaToIdLineCible};
my %lemmaToIdLineSource     = %{$lemmaToIdLineSource};
my %tokenCibleToFuzzySource = %{$tokenCibleToFuzzySourcePm};
my %lemmaCibleToFuzzySource = %{$lemmaCibleToFuzzySource};


if($params{"patterns"} == 1){
  my $startPattern  = time();
  print color("green"), "\nRecherche via les patterns...\n", color("reset") ;

  my @keysCible     = sort { $a <=> $b } keys %idToLineCible;
  my $strTokens     = "";
  my $strTags       = "";
  my $strLemmas     = "";
  my $linesTT       = "";

  foreach my $keyCible (@keysCible){
    my $currentTag   = $idToLineCible{$keyCible}{"tags"};
    my $currentToken = $idToLineCible{$keyCible}{"token"};
    my $currentLemma = $idToLineCible{$keyCible}{"lemma"};
    $strTokens  = $strTokens." ".$currentToken;
    $strTags    = $strTags." ".$currentTag;
    $strLemmas  = $strLemmas." ".$currentLemma;
    $linesTT   .= "<".$currentToken."\t".$currentTag."\t".$currentLemma.">";
    if($currentTag eq "sent"){
      my $scorePattern = StringManager::sentenceMatchPattern($linesTT, \%params);
      if($scorePattern > 0){
        $hitsPerQuotePattern{$strTokens} = $scorePattern;
      }
      $strTokens  = "";
      $strTags    = "";
      $strLemmas  = "";
      $linesTT    = "";
    }
  }
  my $endPattern  = time();
  my $patternTime = $endPattern - $startPattern;
  print color("green"),"Recherche via les patterns finie en ".int($patternTime)." secondes\n\n", color("reset");
}


########################sentenceMatch########## DEBUT LECTURE LIGNE PAR LIGNE CIBLE ############
my $cptBeforeIdsAllin = 0;

if($params{"slidingWindow"} == 1){
  my @slidingWindow;
  my $endOfLine = 0;
  my $nbhits    = 0;
  my $cpt       = 0;
  for (my $currentIndex = 0; $currentIndex < $#cibleLines; $currentIndex++) {
    displayProgress($currentIndex);
    my ($token, $tags, $lemmas, $isFullWord, $isWord) = IndexManager::getTripletsFromId($currentIndex, "cible");
    if(IndexManager::countWords(@slidingWindow) >= $params{"windowSize"}) {
      my $prefixeEmpan = shift(@slidingWindow);
      my ($tokenPrefix, $tagsPrefix, $lemmasPrefix, $isFullWordPrefix, $isWordPrefix) = IndexManager::getTripletsFromId($prefixeEmpan, "cible");
      if($isFullWordPrefix){
        push(@slidingWindow, $currentIndex);
      }
      else{
        $currentIndex--;
      }
    }
    else{
      push(@slidingWindow, $currentIndex);
    }

    my %idsAllin;
    undef %idsAllin;
    # if (IndexManager::countWords(@slidingWindow) >= $params{"windowSize"} - 1) {
    if (IndexManager::countWords(@slidingWindow) >= $params{"windowSize"}) {
      # pour chaque $slidingWindow[mot], on utilise l'index fuzzy pour chercher dans la source $lemmaCibleToFuzzySource{$wordCible}{$wordSource}
      foreach my $slidingWindowEl (@slidingWindow) {
        my ($token, $tags, $lemmas, $isFullWordSliding, $isWordSliding) = IndexManager::getTripletsFromId($slidingWindowEl, "cible");
        # %sentenceKeys > Id des phrases candidates
        my %sentenceKeys;
        # on ne check que les mots pleins
        if($isWordSliding){
          # On récupère les phrases dans lesquelles le lemma (ou un fuzzy) est présent
          my @subLemmas = split(/\|/,$lemmas);
          my $currentTokenHasMatched = 0;
          for my $subLemma (@subLemmas) {
            my @lemmasSource = keys %{$lemmaCibleToFuzzySource{$subLemma}};
            for my $lemmaSource (@lemmasSource) {
                my @keysIds = keys %{ $lemmaToIdLineSource{$lemmaSource} } ;
                for my $keyId (@keysIds) {
                  $sentenceKeys{$keyId} = 1;
                  $currentTokenHasMatched = 1;
                }
            }
          }
          # Si ça n'avait pas matché pour le lemma on récupère les phrases dans lesquelles le token (ou un fuzzy) est présent
          if(!$currentTokenHasMatched && exists($tokenCibleToFuzzySource{$token})){
            for my $keyTokenSource (keys %{$tokenCibleToFuzzySource{$token}}) {
                my @sentencesSourceMatched = keys %{$tokenToIdLineSource{$keyTokenSource}};
                foreach my $sentenceSourceMatched (@sentencesSourceMatched) {
                  $sentenceKeys{$sentenceSourceMatched} = 1;
                }
            }
          }
          for my $tmpKey (keys %sentenceKeys){
            $idsAllin{$tmpKey} = (exists( $idsAllin{$tmpKey} )) ? $idsAllin{$tmpKey} + 1 : 1;
          }
        }
      }
      my $nbEl = 0;
      my @slidingWindowTemp;
      #pour chaque ligne de la source avec au moins autant de match que de mots dans la fenetre glissante
      $cptBeforeIdsAllin ++;
      foreach my $keyIdsAllin ( keys %idsAllin ) {
          @slidingWindowTemp = @slidingWindow;
          if ( $idsAllin{$keyIdsAllin} >= (IndexManager::countWords(@slidingWindowTemp) - 1) ) {
              #tous les mots pleins de l'empan sont dans la ligne > élargir match empan
              my $extendOK = 1;
              my $idExtent = $currentIndex;
              my $giveExtentAChance = $params{"jokerExtent"};
              while ($extendOK) {
                  if ( exists( $cibleLines[ $idExtent + 1 ] ) ) {
                    my $newLine = $cibleLines[ $idExtent + 1 ];
                    my ($newToken, $newTags, $newLemmas, $isFullWordNew, $isWordNew) = IndexManager::getTripletsFromId($idExtent + 1,"cible");
                    my @lemmas = split(/\|/,$newLemmas);
                    my $extensionPossible = 0;
                    if(!$isWordNew){
                      $extensionPossible = 1;
                    }
                    else{
                      foreach my $lemma (@lemmas) {
                        #tous les fuzzies du $lemma; est-ce keysIds
                        my @lemmasSource = keys(%{$lemmaCibleToFuzzySource{$lemma}});
                        foreach my $lemmaSource (@lemmasSource) {
                          if (exists($lemmaToIdLineSource{$lemmaSource}{$keyIdsAllin})){
                            $extensionPossible = 1;
                            last;
                          }
                        }
                      }
                      if(!$extensionPossible){
                        my @tokensSource = keys(%{$tokenCibleToFuzzySource{$newToken}});
                        foreach my $tokenSource (@tokensSource) {
                          if (exists($tokenToIdLineSource{$tokenSource}{$keyIdsAllin})){
                            $extensionPossible = 1;
                            last;
                          }
                        }
                      }
                    }

                    if ($extensionPossible || $giveExtentAChance > 0 ){
                        push( @slidingWindowTemp, $idExtent + 1 );
                        $idExtent++;
                        $giveExtentAChance--;
                    }
                    else {
                        $extendOK = 0;

                        my @linesFromSource = getOrderedSentence($keyIdsAllin);
                        my $strToPrint = IndexManager::linesToToken("cible", \@slidingWindowTemp);
                        my $contexteToPrint = contexteMatch("tokens", @slidingWindowTemp);
                        # my ($matchingSource, $score, $ratioMatch, $exactMatch, $startEmpan, $endEmpan, $hasMatchedAPattern) = cibleSource2score($keyIdsAllin, $idExtent, @slidingWindowTemp);
                        my ($matchingSource, $score, $ratioMatch, $exactMatch, $startEmpan, $endEmpan, $hasMatchedAPattern) = cibleSource2score($keyIdsAllin, @slidingWindowTemp);
                        my $alreadyThere = 0;
                        my @keysIdsCibleToScore = keys (%idsCibleToScore);
                        foreach my $keyIdsCibleToScore (@keysIdsCibleToScore) {
                          my ($keyIdStart, $keyIdEnd) = split("/#/",$keyIdsCibleToScore);
                          if( ( ($keyIdStart >= $startEmpan && $keyIdStart <= $endEmpan) || ($keyIdEnd >= $startEmpan && $keyIdEnd <= $endEmpan) ) && ($idsCibleToScore{$keyIdsCibleToScore} > $score) ){
                            $alreadyThere = 1;
                          }
                        }
                        if ( ($score + $exactMatch >= $params{"minScore"}) && !$alreadyThere ){
                          $nbhits++;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"cible"}     = $strToPrint;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"contexte"}  = $contexteToPrint;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"source"}    = $matchingSource;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"ratio"}     = $ratioMatch;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"score"}     = $score;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"idstart"}   = $startEmpan;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"idend"}     = $endEmpan;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"exactMatch"} = $exactMatch;
                          $hitsInSource{$keyIdsAllin}{$nbhits}{"hasMatchedAPattern"} = $hasMatchedAPattern;
                          $idsCibleToScore{$startEmpan."#".$endEmpan} = $score;
                        }

                        while ( (IndexManager::countWords(@slidingWindowTemp)) > $params{"windowSize"} ) {
                            my $suffixeEmpan = pop(@slidingWindowTemp);
                            my ($lastToken, $lastTags, $lastLemmas, $isFullWordLast, $isWordLast) = IndexManager::getTripletsFromId($suffixeEmpan,"cible");
                        }
                    }
                  }
                  else {
                      $extendOK = 0;
                      my @linesFromSource = getOrderedSentence($keyIdsAllin);
                      #TODO Appel SUB cibleSource2score($keyIdsAllin, @slidingWindowTemp) dans ce cas la
                  }
              }#end while(extendOK)
          } #end if $idsAllin{$keyIdsAllin} >= (IndexManager::countWords(@slidingWindowTemp))
      }#end foreach my $keyIdsAllin ( keys %idsAllin )
    }#end if (IndexManager::countWords(@slidingWindow) >= $params{"windowSize"}) {
  }#end for (my $currentIndex = 0; $currentIndex < $#cibleLines

  ################################## FIN LECTURE CIBLE ###############

  my @keysSentMatch = keys(%hitsInSource);
  foreach my $keySentMatch (@keysSentMatch) {
    my @keysHits = keys(%{$hitsInSource{$keySentMatch}});
    foreach my $keyHits (@keysHits) {
      my $ratio     = $hitsInSource{$keySentMatch}{$keyHits}{"ratio"};
      my $source    = $hitsInSource{$keySentMatch}{$keyHits}{"source"};
      my $cible     = $hitsInSource{$keySentMatch}{$keyHits}{"cible"};
      my $idstart   = $hitsInSource{$keySentMatch}{$keyHits}{"idstart"};
      my $idend     = $hitsInSource{$keySentMatch}{$keyHits}{"idend"};
      my $exactMatch= $hitsInSource{$keySentMatch}{$keyHits}{"exactMatch"};
      my $score     = $hitsInSource{$keySentMatch}{$keyHits}{"score"} + $exactMatch;
      my $hasMatchedAPattern = $hitsInSource{$keySentMatch}{$keyHits}{"hasMatchedAPattern"};

      if ($ratio >= $params{"minRatio"}) {
        my $addCurrent = 1;
        if(exists($oneHitPerSent{$keySentMatch})){
          my @keysMatches = keys (%{$oneHitPerSent{$keySentMatch}});
          foreach my $keysMatch (@keysMatches){
            my ($lastIdstart, $lastIdend) = split(/-/,$keysMatch);
            if( ( ($idstart >= $lastIdstart) && ($idstart <= $lastIdend) ) || ( ($idend >= $lastIdstart) && ($idend <= $lastIdend) ) ){
              my $keyHitPrevious = $oneHitPerSent{$keySentMatch}{$keysMatch};
              my $previousScore = $hitsInSource{$keySentMatch}{$keyHitPrevious}{"score"} + $hitsInSource{$keySentMatch}{$keyHitPrevious}{"exactMatch"};
              my $previousRatioScore = $previousScore * $hitsInSource{$keySentMatch}{$keyHitPrevious}{"ratio"};
              if($score*$ratio < $previousRatioScore ){
                $addCurrent = 0;
              }
              else{
                delete $oneHitPerSent{$keySentMatch}{$keysMatch};
              }
            }
          }
        }
        if ($addCurrent == 1) {
          $oneHitPerSent{$keySentMatch}{$idstart."-".$idend} = $keyHits;
        }
      }
    }
  }
}

ExportManager::exportHTML(\$prefixFileOut, \$cibleFile, \%oneHitPerSent, \%hitsPerQuotePattern, \%hitsInSource, \%params);

close(LOG);
my $end_run = time();
my $run_time = $end_run - $start_run;
print "Temps total: ".int($run_time)." secondes\n";
print " -- pour construire : $cptBeforeIdsAllin empans\n";
print "\n\nhtml > file://".cwd."/".$params{"repResults"}."/".$prefixFileOut."-results.html \n\n";

sub contexteMatch{
  my ($target, @slidingWindowTemp) = @_;
  my $strToPrint    = "";
  my $leftBorder    = $slidingWindowTemp[0];
  my $rightBorder   = $slidingWindowTemp[$#slidingWindowTemp-1];
  my $leftContext   = ($leftBorder - $params{"contexteCible"} < 0) ? 0 : $leftBorder - $params{"contexteCible"};
  my $rightContext  = ($rightBorder + $params{"contexteCible"} > $#cibleLines) ? $#cibleLines : $rightBorder + $params{"contexteCible"};
  for (my $var = $leftContext; $var <= $rightContext; $var++) {
    my ($token, $tags, $lemma, $isFullWord, $isWord) = IndexManager::getTripletsFromId($var, "cible");
    if ($target eq "tokens") {
       $strToPrint .= $token." ";
    } elsif($target eq "lemmas"){
      $strToPrint .= $lemma." ";
    } elsif($target eq "tags"){
      $strToPrint .= $tags." ";
    } elsif($target eq "fullLine"){
      $strToPrint .= "<".$token."\t".$tags."\t".$lemma.">";
    }
  }

  return $strToPrint;
};

sub getOrderedSentence{
  my ($idSentenceSource) = @_;

  my @cptOrdons = sort ( {$a <=> $b} keys ( %{$idToLineSource{$idSentenceSource}} ) );
  my @sentenceSource;
  foreach my $cptOrdon (@cptOrdons) {
    push(@sentenceSource, $idToLineSource{$idSentenceSource}{$cptOrdon}{"idSource"});
  }

  return (@sentenceSource);
}

sub cibleSource2score{
  $calculScoreIteration++;
    #$idSentenceSource -> id de la phrase source
    #@empanCible -> tableau de ligne de la fenetre glissante en cours (1 ligne = 1 ligne treetagger)

  #output :
    #$score -> distance Levenshtein + pondération si présence Entités nommées, Quote. Si matching exact -> score max
    #$matchingSource -> string pour affichage de la phrase source avec emphase du segment qui match (<citation>es, quos saecula prisca tulerunt</citation>)

  #processus :
    #a minima -> récupérer l'id min de la première occurrence dans source d'un mot de @empanCible, pareil pour id max
            # -> calcul du score et présentation des résultats

   my ($idSentenceSource, @empanCible) = @_;
   my @toto = getOrderedSentence($idSentenceSource);
   print  LOG "source = ".join(" ", IndexManager::linesToToken("source", \@toto))."\n";

   my $empanSize      = $#empanCible + 1;
   # my $start          = $idLastMatchedTokenCible - $empanSize;
   my $start          = $empanCible[0];
   my $idLastMatchedTokenCible = $empanCible[$#empanCible] + 1;
   my $score          = 0;
   my $nbHits         = 0;
   my $matchingSource = "";
   my @sentenceSource = getOrderedSentence($idSentenceSource);
   my $cptSource = 0;
   my @idMatched;

   my %tmpLemmaCountToIdSource = %{$lemmaCountToIdSource{$idSentenceSource}};#sert à ne pas matcher plus de n fois un élément présent n fois dans la cible

   #TODO -> le $tmpLemmaCountToIdSource doit prendre en compte aussi les tokens pour permettre 1 reconnaissance autre que sur lemme
   my %tmpTokenCountToIdSource = %{$idSourceToTokensToCount{$idSentenceSource}};
   my %idCibleHasMatched; #en clé id/rang dans le tableau @empanCible / en valeur 1 si déjà matché.
   my $lastSourceMatched = 1;
   my @sentenceSourceExtended;
   my $wordSourceExtended = 0;

   #print  "empan cible = ".join(" ", IndexManager::linesToToken("cible", \@empanCible))."\n";
   while($lastSourceMatched){
     $lastSourceMatched = 0;
     my $cptSource = 0;
     foreach my $wordSource (@sentenceSource) {
       my ($sourceToken, $sourceTags, $sourceLemma, $isFullWordSource, $isWordSource) = IndexManager::getTripletsFromId($wordSource,"source");
       my $toEmphatize  = 0;
       my $cptCible     = 0;
       $wordSourceExtended++;
       foreach my $wordCible (@empanCible) {
         my $hasMatched = 0;
         my ($tokenCible, $tagsCible, $lemmaCible, $isFullWordCible, $isWordCible) = IndexManager::getTripletsFromId($wordCible,"cible");
         if( $isWordSource || $isWordCible ){
           # !!!! test qu'un mot cible ne score qu'une fois
           if (($tmpLemmaCountToIdSource{$sourceLemma} && $tmpLemmaCountToIdSource{$sourceLemma} > 0) || ($tmpTokenCountToIdSource{$sourceToken} && $tmpTokenCountToIdSource{$sourceToken} > 0)) {
                  my $increment = 0;
                  if($tokenCible eq $sourceToken){
                     $increment = $params{"pointTokenEq"};
                     $hasMatched = 1;
                     $tmpTokenCountToIdSource{$sourceToken}--;
                  }
                  # exact lemmas match
                  elsif($lemmaCible eq $sourceLemma){
                    $increment = $params{"pointLemmaEq"};
                    $hasMatched = 1;
                    $tmpLemmaCountToIdSource{$sourceLemma}--;
                  }
                  # fuzzy token match
                  elsif(exists($tokenCibleToFuzzySource{$tokenCible}{$sourceToken})){
                    $increment = $params{"pointTokenFuzz"};
                    $hasMatched = 1;
                    $tmpTokenCountToIdSource{$sourceToken}--;
                  }
                  # fuzzy lemmas match
                  elsif(exists($lemmaCibleToFuzzySource{$lemmaCible}{$sourceLemma})){
                    $increment = $params{"pointLemmaFuzz"};
                    $hasMatched = 1;
                    $tmpLemmaCountToIdSource{$sourceLemma}--;
                  }

                  if ($hasMatched) {
                    if ($cptSource == $#sentenceSource) {
                        $lastSourceMatched = 1;
                    }
                    if($wordCible > $idLastMatchedTokenCible){
                      $idLastMatchedTokenCible = $wordCible;
                    }
                    push(@idMatched,$wordSourceExtended);
                    $toEmphatize = 1;
                    if(!exists($idCibleHasMatched{$cptCible})){
                      $nbHits++;
                      $score = $score + $increment;
                      $idCibleHasMatched{$cptCible} = $increment;
                    }
                    elsif($idCibleHasMatched{$cptCible} < $increment){
                      $score = $score + ($increment - $idCibleHasMatched{$cptCible});
                    }
                    last;
                  }
              }
          }
        $cptCible++;
        if( !$toEmphatize && ((!$isFullWordSource && $isWordSource) || (!$isFullWordCible && $isWordCible)) && ($sourceToken eq $tokenCible)){
            $toEmphatize = 1 ;
            $score = $score + $params{"pointEmptyWord"};
        }
      }
      $matchingSource.= ($toEmphatize) ? " <el>".$sourceToken."</el>" : " ".$sourceToken;
      $cptSource++;
      }
      foreach my $wordSource (@sentenceSource) {
        push(@sentenceSourceExtended,$wordSource);
      }
      if($lastSourceMatched && exists($idToLineSource{$idSentenceSource + 1}) ){
        $idSentenceSource++;
        @sentenceSource = getOrderedSentence($idSentenceSource);
        %tmpLemmaCountToIdSource = %{$lemmaCountToIdSource{$idSentenceSource}};#sert à ne pas matcher plus de n fois un élément présent n fois dans la cible
        %tmpTokenCountToIdSource = %{$idSourceToTokensToCount{$idSentenceSource}};
      }
      else{
        $lastSourceMatched = 0;
      }
   }

   $score = (IndexManager::aNamedEntityIsNotThatFar($idLastMatchedTokenCible, $start, $params{"distanceNE"})) ? $score + $params{"bonusEntity"} : $score;
   $score = (IndexManager::isBetweenQuotes($idLastMatchedTokenCible, $start, $params{"distanceQuote"})) ? $score + $params{"bonusQuote"} : $score;
   my $hasMatchedAPattern = StringManager::sentenceMatchPattern(contexteMatch("fullLine", @empanCible), \%params);
   # $matchingSource=~s/^(.+<\/el>).+$/$1/;

   my $stringWindow = IndexManager::linesToToken("cible", \@empanCible);
   my $stringSource = IndexManager::linesToToken("source", \@sentenceSourceExtended);
   my $exactMatch = StringManager::exactMatch($stringWindow, $stringSource, $params{"bonusExactMatch"});

   my $last = undef;
   my $sumGap = 0;
   foreach my $index (@idMatched) {
     my $sumStopWords = 0;
     my ($sourceToken,$sourceTags,$sourceLemma,$isFullWordSource, $isWordSource) = IndexManager::getTripletsFromId($index, "source");
      if (defined $last) {
        for (my $j = $last; $j <= $index; $j++) {
          my ($tmpTok, $tmpTag, $tmpLemma, $tmpIsFullWordSource, $tmpIsWordSource) = IndexManager::getTripletsFromId($j, "source");
          if ($tmpIsWordSource) {
            $sumStopWords++;
          }
        }
        # $sumGap = $sumGap + ($index - 1 - $last) - $sumStopWords;
        $sumGap = $sumGap + ($index - 1 - $last);
      }
      $last = $index;
   }
   $score -= $params{"gapFactor"} * $sumGap;
   $matchingSource= "<match>".$matchingSource."</match>";
   my $ratioMatch = $nbHits / IndexManager::countWords(@empanCible) * 100;
   $ratioMatch = sprintf("%.2f",$ratioMatch);

    if ($hasMatchedAPattern < 0) {
      $score      = 0;
      $ratioMatch = 0;
      $exactMatch = 0;
    }

   return ($matchingSource, $score, $ratioMatch, $exactMatch, $start, $idLastMatchedTokenCible, $hasMatchedAPattern);
}

sub displayProgress{
  my ($currentIndex) = @_;

  if($params{"displayProgress"} == 1){
    $progress = $currentIndex * 100 / $#cibleLines;
    if ($progress > $oldProgress + 5) {
      $oldProgress = $progress;
      print color("green"), "Parsing cible... ".int($progress)."%\n", color("reset");
      if ($progress > 0){
        my $currentTime = time();
        my $currentRuntime = $currentTime - $start_run;
        my $eta = int(100 / $progress * $currentRuntime);
        print color("blue"),  "Temps restant estime : ".int($eta - $currentRuntime)." sec. \n", color("reset");
      }
    }
  }
}
