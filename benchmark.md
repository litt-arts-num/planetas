# Benchmark
  ```
  pour un réglage donné :
  * bruit supplantant les attendus
  * rang des attendus (par ratio ou par score)
  * segmentation des contextes cibles (englobe ou pas l'intégralité)
  ```

# 23/03/2020
  ```
  Résultats après refonte pour n phrases
    * Eutyches -> tous les quotes expected sont en début
    * Cledonii -> tous les quotes expected sont en début
    * Phocas ->
      - "finibus extremis" segmenté et bas
      - "degener o populus" pas matché
    * Diomede ->
      - manque "si funere primo" à une quote
      - uittata sacerdos trop bas
    * Servius -> paraît OK
    * Cavajoni -> pas d'attendu
  ```

  /	ADJ	<unknown>
  sed	CC	sed
  noua	ADJ	novus
  te	PRON	tu
  breuitas	N:nom	brevitas
  asserit	V:IND	assero
  esse	ESSE:INF	sum
  meam	POSS	meus
  .	SENT	.
  /	N:voc	<unknown>
  omnia	PRON	omnis

## Paramètres stables
```
my $minLevenstein     = 20;  
our $distanceNE       = 8;   
our $distanceQuote    = 6;
our $minRatio         = 65;
our $bonusEntity      = 5;
our $bonusQuote       = 8;
our $bonusExactMatch  = 25;
our $pointTokenEq     = 5;
our $pointTokenFuzz   = 3;
our $pointLemmaEq     = 4;
our $pointLemmaFuzz   = 3;
our $jokerExtent      = 1;
our $contexteCible    = 5;
our $minScore         = 9;
```

## CLEDONII
`perl create-index-latin-tt-v2.pl cledonii.txt`

> windowSize = 2 | gapFactor = 2 -> hits aux premiers rangs
> windowSize = 3 | gapFactor = 3 -> hits aux premiers rangs & bruit mieux isolé

## DIOMEDE
`perl create-index-latin-tt-v2.pl diomede.txt`

> windowSize = 3 | gapFactor = 2
> uittata sacerdos (18) trop loin / 2 autres hits aux premiers rangs
> windowSize = 2 | gapFactor = 2 -> paraît plus adapté
> uittata sacerdos (18) moins loin / 2 autres hits aux premiers rangs

## EUTYCHES
`perl create-index-latin-tt-v2.pl eutyches.txt`

> windowSize = 2 | gapFactor = 2 -> hits aux premiers rangs
> windowSize = 3 | gapFactor = 3 -> hits aux premiers rangs & bruit mieux isolé

## PHOCAS
`perl create-index-latin-tt-v2.pl phocas.txt`

> windowSize = 3 | gapFactor = 2
> tous les hits dans les premiers rangs (juste un bruit - populus : sic lucanus ")
> windowSize = 2 | gapFactor = 2 -> paraît plus adapté
>

## CAVAJONI
`perl create-index-latin-tt-v2.pl cavajoni.txt`

------------------

## Performance

  - test sur eutyches (87 kB): fuzzy lemma 575 / fuzzy token 1854 / parsing 195  (30 secondes avec ws 3)/   split 57/  score 141
  - test sur cledonii (153 kB): fuzzy lemma 570 / fuzzy token 2064 / parsing 489 (66 avec ws 3 )/   split 15 /  score 357 /
  - test sur diomede-short (2.9 kB): 8.2 parsing /  4.3 split / 6.4 score / 4.9 foreach
  - test sur phocas (62.4 kB): 240 parsing /  120 split / 192 score / 147 foreach
  - test sur diomede / window - 1 modifié (469.9 kB): 273.9 parsing /  89.3 split / 1220 score / 784 foreach

## Questions à poser à FB
  - utilisation "libro  xx" ?
  - entités nommées avant la citation ?

## ISSUES

- préparation des données à partir des doc (suppression ou reformatage des <>, ex: <singulare> dans cledonii)
- emphatisation des mots vides ?

- source : finibus extremis libyes , ubi feruida tellus
- cible Diomede : « finibus extremis libyes ubi feruida tellus
  - problème sur ubi pas matché (faute de la virgule ? stop word ?)
- source :  « traxerunt in arte magica uertigine fili » ; et haec fila
- cible phocas : quos non concordia mixti alligat ulla tori blandaeque potentia formae , traxerunt torti magica uertigine fili
  - problème sur torti remplacé par in arte (ajout +1 à joker)
