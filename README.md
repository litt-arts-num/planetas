# PLANETAS

Programme d'aide à la récupération de citations en latin...

## Install

``` 
# Linux
make install

# windows 
## télécharger l'archive du code et la dézipper
https://gitlab.com/litt-arts-num/planetas/-/archive/master/planetas-master.zip
## installer perl
http://strawberryperl.com/download/5.30.2.1/strawberry-perl-5.30.2.1-64bit.msi
## installer les modules Text::Trim et Text::Levenshtein 
cpan Text::Trim
cpan Text::Levenshtein 
## todo > install treetagger
``` 


## Utilisation

```
# se placer dans le répertoire préablablement dézippé
perl call-tt-latin.pl (pas sous windows pour le moment)
perl create-index-latin-tt-v2.pl nomdufichiercible.txt (dans input/txt/)
```

## Paramètres
```
bonusEntity     = 10  # bonus de points lié à la proximité d'une entité nommée
bonusExactMatch = 25  # bonus de points lié à la présence exacte de la fenêtre glissante dans la source
bonusQuote      = 10  # bonus de points lié à la proximité de guillemets
contexteCible   = 5   # nombre de tokens dans les contextes gauche et droite qui seront affichés
distanceNE      = 8   # distance max pour déclencher le bonus de proximité d'entité nommée
distanceQuote   = 4   # distance max pour déclencher le bonus de proximité de guillemets
gapFactor       = 2   # facteur pour le malus de points lié à la dispersion des mots qui matchent
jokerExtent     = 1   # booléen pour ajouter ou non une tolérance pour l'extension de la fenêtre glissante
minLevenstein   = 20  # pourcentage maximal de variation pour une forme (en fonction de sa longueur)
minRatio        = 65  # pourcentage minimal de mots pleins matchant dans la fenêtre glissante pour qu'un résultat soit conservé
minScore        = 9   # score minimal pour qu'un match soit conservé
pointLemmaEq    = 5   # points pour un lemme matchant exactement
pointLemmaFuzz  = 3   # points pour un lemme matchant avec un lemme approximant (voir minLevenstein)
pointTokenEq    = 5   # points pour un token matchant exactement
pointTokenFuzz  = 4   # points pour un token matchant avec un token approximant (voir minLevenstein)
windowSize      = 3   # empan/fenêtre glissante de recherche (en nombre de tokens)
displayProgress = 1   # booléen. Affichage ou non de la progression de la fenêtre glissante.
slidingWindow   = 1   # booléen. Recherche via fenêtre glissante.
patterns        = 1   # booléen. Recherche via les patterns.
```

## Veille
* DeSert, Ganascia (obvil)
  - pas le latin
  - pas les sources
* Textreuse (Anne teste)
* Logiciel je sais plus comment il s'appelle de Guillemin. -> fait pas ce qu'il faut et en plus écrit en OCaml
