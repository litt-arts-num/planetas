clean-all: clean-fuzzy clean-results

install:
	sudo apt-get install perl
	cd ./tt && sh ./install-tagger.sh
	sudo perl -MCPAN -e 'install Text::Trim'
	sudo perl -MCPAN -e 'install Text::Levenshtein'
	cp parameters.conf.dist parameters.conf

clean-fuzzy:
		find ./ressources/ | grep .*.txt | xargs rm -f

clean-results:
		find ./results/ | grep .*.results.html | xargs rm -f
		find ./results/ | grep .*.matches-all.csv | xargs rm -f
