package ExportManager;
use Encode;
use strict;
# use warnings;
use utf8;
use locale;

sub exportHTML{
  my ($prefixFileOut, $cibleFile, $oneHitPerSent, $hitsPerQuotePattern, $hitsInSource, $params) = @_;
  my $idNum      = 0;
  my $maxResults = $$params{"maxResults"};
  my $sourceFile = $$params{"sourceFile"};
  my $repResults = $$params{"repResults"};

  print "Generation du HTML... \n";
  open(HTML, ">:encoding(utf8)", $repResults."/".$$prefixFileOut."-results.html");
  print HTML "
              <!doctype html>
              <html lang='fr'>
                <head>
                  <meta charset='utf-8'>
                  <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
                  <style>el{background-color:lightgrey;} el.empty-word{background-color:#a3a3a3;}</style>
                  <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'/>
                </head>
                <body>
                  <div class='container-fluid'>
                    <h2 class='mt-3'>
                      <mark>$sourceFile</mark> / <mark>$$cibleFile</mark>
                      <button type='button' class='btn btn-info' data-html='true' data-toggle='popover' title='Paramètres' data-content='
                        windowSize      = $$params{'windowSize'} <br/>
                        minLevenstein   = $$params{'minLevenstein'} <br/>
                        distanceNE      = $$params{'distanceNE'} <br/>
                        distanceQuote   = $$params{'distanceQuote'} <br/>
                        minRatio        = $$params{'minRatio'} <br/>
                        bonusEntity     = $$params{'bonusEntity'} <br/>
                        bonusQuote      = $$params{'bonusQuote'} <br/>
                        bonusExactMatch = $$params{'bonusExactMatch'} <br/>
                        pointTokenEq    = $$params{'pointTokenEq'} <br/>
                        pointTokenFuzz  = $$params{'pointTokenFuzz'} <br/>
                        pointLemmaEq    = $$params{'pointLemmaEq'} <br/>
                        pointLemmaFuzz  = $$params{'pointLemmaFuzz'} <br/>
                        gapFactor       = $$params{'gapFactor'} <br/>
                        jokerExtent     = $$params{'jokerExtent'} <br/>
                        contexteCible   = $$params{'contexteCible'} <br/>
                        minScore        = $$params{'minScore'} <br/>
                        maxResults      = $$params{'maxResults'}
                      '>?</button>
                    </h2>

                    <input class='my-4 form-control' id='filter' type='text' placeholder='Filtrer'>

                    <ul class='nav nav-tabs'>
                      <li class='nav-item'>
                        <a class='nav-link active' id='sliding-window-tab' data-toggle='tab' href='#sliding-window' role='tab'>Fenêtre glissante</a>
                      </li>
                      <li class='nav-item'>
                        <a class='nav-link' id='patrons-tab' data-toggle='tab' href='#patrons' role='tab'>Patrons</a>
                      </li>
                    </ul>

                    <div class='tab-content' id='myTabContent'>
                      <div class='tab-pane fade show active' id='sliding-window' role='tabpanel'>
                        <table class='table mt-3'>
                          <thead class='thead-dark'><tr>
                            <th data-sort='int' scope='col'>Rang</th>
                            <th data-sort='int' scope='col'>Id</th>
                            <th data-sort='int' scope='col'>Ratio</th>
                            <th data-sort='int' scope='col'>Score</th>
                            <th data-sort='int' scope='col'>R*S/100</th>
                            <th scope='col'>Cible</th>
                            <th scope='col'>Source</th>
                            <th scope='col'>Contexte</th>
                          </tr>
                        </thead>";

  my @keysTmp = keys (%{$oneHitPerSent});
  my %hitsRanked;
  my $rank = 0;
  foreach my $keyTmp (@keysTmp) {
    # my @empans = keys (%{$oneHitPerSent{$keyTmp}});
    my @empans = keys (%{${$oneHitPerSent}{$keyTmp}});

    foreach my $empan (@empans) {
        $rank++;
        my $htmlStr       = "";
        my $keyHit        = ${$oneHitPerSent}{$keyTmp}{$empan};
        my $score         = sprintf '%.2f', ${$hitsInSource}{$keyTmp}{$keyHit}{"score"};
        my $exactMatch    = sprintf '%.2f', ${$hitsInSource}{$keyTmp}{$keyHit}{"exactMatch"};
        $score            = $score + $exactMatch;
        my $ratio         = ${$hitsInSource}{$keyTmp}{$keyHit}{"ratio"};
        my $source        = ${$hitsInSource}{$keyTmp}{$keyHit}{"source"};
        my $cible         = ${$hitsInSource}{$keyTmp}{$keyHit}{"cible"};
        my $contexte      = ${$hitsInSource}{$keyTmp}{$keyHit}{"contexte"};
        my $idstart       = ${$hitsInSource}{$keyTmp}{$keyHit}{"idstart"};
        my $idend         = ${$hitsInSource}{$keyTmp}{$keyHit}{"idend"};
        my $hasMatchedAPattern  = ${$hitsInSource}{$keyTmp}{$keyHit}{"hasMatchedAPattern"};
        my $class         = ($hasMatchedAPattern > 0) ? "class='table-success'" : "";
        my $ratioScore    = $ratio / 100 * $score;
        $idNum++;
        $htmlStr .= "<td>".$idNum."</td>";
        $htmlStr .= "<td>".$ratio."</td>";
        $htmlStr .= "<td>".$score."</td>";
        $htmlStr .= "<td>".$ratioScore."</td>";
        $htmlStr .= "<td ".$class.">".$cible."</td>";
        $htmlStr .= "<td>".$source."</td>";
        $htmlStr .= "<td ".$class.">".$contexte."</td>";
        $hitsRanked{$htmlStr} = $ratioScore;
    }
  }
  $rank = 0;
  foreach my $key (sort { $hitsRanked{$b} <=> $hitsRanked{$a} } keys %hitsRanked) {
    if($maxResults > 0){
      $rank++;
      print HTML "
      <tr><td>".$rank."</td>".$key."</tr>";
      $maxResults--;
    }
  }

  print HTML '</table>
              </div>';


  print HTML "<div class='tab-pane fade' id='patrons' role='tabpanel'>";
  print HTML "<table class='table mt-3'>";
  print HTML "<thead class='thead-dark'><tr>";
  print HTML "<td>Score</td>";
  print HTML "<td>Cible</td>";
  print HTML "</tr></thead>";

  foreach my $key (sort { ${$hitsPerQuotePattern}{$b} <=> ${$hitsPerQuotePattern}{$a} } keys %{$hitsPerQuotePattern}) {
    print HTML "<tr><td>".${$hitsPerQuotePattern}{$key}."</td>";
    print HTML "<td>".$key."</td></tr>";
  }

  print HTML '</table></div>'; #end sliding-window tab
  print HTML '</div>'; #end tabs content
  print HTML '</div>'; #end container
  print HTML '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/stupidtable/1.1.3/stupidtable.min.js"></script>
              <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
              <script>
              $("[data-toggle=\'popover\']").popover();

              let input = document.getElementById("filter");
              let entries = document.getElementsByTagName("tr");
              let entriesArray = Array.from(entries);
              input.addEventListener("keyup", function() {
                filter(this.value);
              }, false);
              function filter(value) {
                entriesArray.forEach(function(entry) {
                  if (entry.innerHTML.indexOf(value) > -1) {
                      entry.classList.remove("d-none");
                  } else {
                    entry.classList.add("d-none");
                  }
                });
              }
              $("table").stupidtable();
              </script>
            </body>
          </html>';
  close HTML;
}
1;
