package IndexManager;
use strict;
# use warnings;
use utf8;
use locale;
use StringManager;

our @cibleLines;
our %ambLemmaToIdLineCible;   # %lemmaToIdLine{Emathios}{0}= 1;
our %ambLemmaToIdLineSource;  # %lemmaToIdLine{Emathios}{0}= 1;
our %closeQuoteCibleToId;     # %closeQuoteCibleToId{8}= 1;la ligne 8 du fichier cible représente une entité nommée
our %idTokenSourceToValues;
our %idToLineCible;
our %idToLineSource;          # $idToLineSource{$idSentenceSource}{$rangToken}{"token|tag|lemmas|idSource"} = $string
our %lemmaCibleToFuzzySource; # $lemmaCibleToFuzzySource{$word_cible}{$id_word_source}= 1
our %lemmaCountToIdSource;    # %lemmaCountToIdSource{Emathios}{4}=3; Il y a 3 occurrences du lemme Emathios dans la phrase n°4 du fichier source
our %lemmaToIdLineCible;      # %lemmaToIdLine{Emathios}{0}= 1;
our %lemmaToIdLineSource;     # %lemmaToIdLine{Emathios}{0}= 1;
our %neCibleToId;             # %neCibleToId{8}= 1;la ligne 8 du fichier cible représente une entité nommée
our %openQuoteCibleToId;      # %openQuoteCibleToId{8}= 1;la ligne 8 du fichier cible représente une entité nommée
our %tokenCibleToFuzzySource; # $tokenCibleToFuzzySource{$token_cible}{$token_source}= 1
our %idSourceToTokensToCount; # %idSourceToTokensToCount{Emathios}{4}=3; Il y a 3 occurrences du token Emathios dans la phrase n°4 du fichier source
our %tokenToIdLineCible;      # $tokenToIdLineSource{Emathios}{0}=1;
our %tokenToIdLineSource;     # $tokenToIdLineSource{Emathios}{0}=1;
##FILES
our $nameFileOutFuzzyLemma;   # "dictionnaire" mappant les mots de cible à ceux de source
our $nameFileOutFuzzyToken;   # "dictionnaire" mappant les mots de cible à ceux de source
our $cibleFilePath;
our $sourceFilePath;


sub aNamedEntityIsNotThatFar{
  my ($end, $start, $distanceNE) = @_;
  my $rangeMin = $start - $distanceNE;
  my $rangeMax = $start;
  for (my $i = $rangeMin; $i <= $rangeMax; $i++) {
    if (exists($neCibleToId{$i})) {
      return 1;
    }
  }
  return 0;
}


sub countWords{
  my @idsTTLines = @_;
  my $cpt = 0;
  for my $idTTLine (@idsTTLines) {
    my ($token, $tags, $lemmas, $isFullWord, $isWord) = getTripletsFromId($idTTLine, "cible");
    if($isWord){
      $cpt++;
    }
  }
  return $cpt;
}

sub isBetweenQuotes{
  my ($end, $start, $distanceQuote) = @_;
  my $rangeMin = $start - $distanceQuote;
  my $rangeMax = $end + $distanceQuote;
  my $matchStart = 0;
  my $idStart;
  my $idEnd = 0;

  for (my $i = $rangeMin; $i <= $end; $i++) {
    if (exists($openQuoteCibleToId{$i})) {
      $matchStart = 1;
      $idStart=$i;
      last;
    }
  }
  if ($matchStart) {
    for (my $i = $idStart; $i <= $rangeMax; $i++) {
      if (exists($closeQuoteCibleToId{$i})) {
        $idEnd = $i;
      }
    }
    if( ($idStart == $idEnd) || ($idEnd == $start) ){
      return 0;
    }
    elsif( ($idStart < $start && $idEnd > $start) || ($idStart >= $start && $idEnd > $idStart) ){
      return 1;
    }
  }

  return 0;
}

sub linesToToken{
  my ($where, $linesTT) = @_;
  my $strToPrint = "";
  foreach my $line (@{$linesTT}){
    my ($token, $tags, $lemma, $isFullWord, $isWord) = getTripletsFromId($line, $where);
     $strToPrint .= $token." ";
  }

  return $strToPrint;
};

sub getTripletsFromId{
  my ($idToken, $where) = @_;
  # if( $where ne "cible" && $where ne "source"){
  #   print "Where $where \n";
  # }
  my ($token, $tags, $lemmas, $isFullWord, $isWord);
  if ($where eq "source") {
    $token      = $idTokenSourceToValues{$idToken}{"token"};
    $tags       = $idTokenSourceToValues{$idToken}{"tags"};
    $lemmas     = $idTokenSourceToValues{$idToken}{"lemma"};
    $isFullWord = $idTokenSourceToValues{$idToken}{"isFullWord"};
    $isWord     = $idTokenSourceToValues{$idToken}{"isWord"};
  } else {
    $token      = $idToLineCible{$idToken}{"token"};
    $tags       = $idToLineCible{$idToken}{"tags"};
    $lemmas     = $idToLineCible{$idToken}{"lemma"};
    $isFullWord = $idToLineCible{$idToken}{"isFullWord"};
    $isWord     = $idToLineCible{$idToken}{"isWord"};
  }

  return ($token, $tags, $lemmas, $isFullWord, $isWord);
}

sub launchAndGetVariables{
  my ($cibleFile, $params) = @_;
  my $sourceFile          = $$params{"sourceFile"};
  my $minLevenstein       = $$params{"minLevenstein"};
  $cibleFilePath          =  "input/tt/".$cibleFile;
  $sourceFilePath         =  "input/tt/".$sourceFile;
  $nameFileOutFuzzyLemma  = "ressources/fuzzy-lemma-".$minLevenstein."-".$cibleFile; #"dictionnaire" mappant les mots de cible à ceux de source
  $nameFileOutFuzzyToken  = "ressources/fuzzy-token-".$minLevenstein."-".$cibleFile; #"dictionnaire" mappant les mots de cible à ceux de source

  handleSourceIndexes($sourceFilePath);
  handleCibleIndexes($cibleFilePath);
  my $start_run = time();
  makeFuzzyLemmas($minLevenstein);
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Fuzzy Lemmas took $run_time seconds\n";

  $start_run = time();
  makeFuzzyTokens($minLevenstein);
  $end_run = time();
  $run_time = $end_run - $start_run;
  print "Fuzzy Tokens took $run_time seconds\n";

  $start_run = time();
  readFuzzy("lemma");
  $end_run = time();
  $run_time = $end_run - $start_run;
  print "Read Fuzzy lemmas took $run_time seconds\n";

  $start_run = time();
  readFuzzy("token");
  $end_run = time();
  $run_time = $end_run - $start_run;
  print "Read Fuzzy Tokens took $run_time seconds\n";

  return (\%idToLineCible, \%idToLineSource, \%idSourceToTokensToCount , \%tokenToIdLineCible, \%tokenToIdLineSource, \%lemmaCountToIdSource, \%lemmaToIdLineCible, \%lemmaToIdLineSource, \@cibleLines, \%tokenCibleToFuzzySource, \%lemmaCibleToFuzzySource);
}

# our ($idToLineCible, $idToLineSource, $idSourceToTokensToCount , $tokenToIdLineCible, $tokenToIdLineSource, $lemmaCountToIdSourcePm, $lemmaToIdLineCible, $lemmaToIdLineSource, $cibleLines, $tokenCibleToFuzzySourcePm, $lemmaCibleToFuzzySource) =

sub handleCibleIndexes{
  my ($cibleFilePath) = @_;

  my $start_run = time();
  open( CIBLE, "<:encoding(utf8)", $cibleFilePath );
  my $cptLines = 0;
  my $currentId = 0;
  while ( ( my $line = <CIBLE> ) ) {
    if($line=~/^[^\t]+\t[^\t]+\t[^\t]+$/){

      $line = lc $line;
      # $line = strip_tags( $line);
      my ($token,$tags,$lemma) = StringManager::splitLineTab($line);

      # if($line =~/^([A-Z][\t]*?)\t(NPR|N:).+?\1$/){
      if($lemma =~/^(l[uv]can.+)|(lib.r)|([ixv]+)|(dico)|(aio)$/){
        $neCibleToId{$cptLines} = 1;
      }
      elsif($token =~/^[«“‘‹"'<]$/){
        $openQuoteCibleToId{$cptLines} = 1;
      }
      elsif($token =~/^[»”’›"'>]$/){
        $closeQuoteCibleToId{$cptLines} = 1;
      }
      if($tags eq "sent"){
        $currentId++;
      }
      else{
        $tokenToIdLineCible{$token}{$currentId}=1;
        $ambLemmaToIdLineCible{$lemma}{$currentId} = 1;
        # if($lemma=~/^(.+\|.+)$/){
        if(index($lemma,"|")  != -1){
          #si amb (a|b) alors $lemmaToIdLineSource{(a|b)}{$currentId}=1
          #les currentId sont l'union des currentId de a et b
          my %idsAmbs;
          my @ambLemmas = split(/\|/,$lemma);
          foreach my $ambLemma (@ambLemmas){
            $lemmaToIdLineCible{$ambLemma}{$currentId} = 1;
            foreach my $tmpkey (keys %{$lemmaToIdLineCible{$ambLemma}}) {
              $idsAmbs{$tmpkey} = 1;
            }
          }
          foreach  my $tmpKeyAmb (keys %idsAmbs) {
            $lemmaToIdLineCible{$tmpKeyAmb}{$currentId} = 1;
          }
        }
        else{
          $lemmaToIdLineCible{$lemma}{$currentId} = 1;
        }
      }
      push( @cibleLines, $cptLines);
      $idToLineCible{$cptLines}{"token"}      = $token;
      $idToLineCible{$cptLines}{"tags"}       = $tags;
      $idToLineCible{$cptLines}{"lemma"}      = $lemma;
      $idToLineCible{$cptLines}{"isFullWord"} = StringManager::isFullWord(\$lemma, \$tags);
      $idToLineCible{$cptLines}{"isWord"}     = StringManager::isWord(\$lemma, \$tags);

      $cptLines++;
    }
  }
  close(CIBLE);
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  my @keysCible = sort { $a <=> $b } keys %idToLineCible;
  print "Creation index Cible took $run_time seconds\n";
}

sub handleSourceIndexes{
  my ($sourceFilePath) = @_;
  open( SOURCE, "<:encoding(utf8)", $sourceFilePath);

  my $start_run = time();

  my $line;
  my $currentId = 0;
  my $cptToken=0;
  my $cptHashSource=0;
  while ( ( $line = <SOURCE> ) ) {
      $line = lc $line;
      if($line=~/^[^\t]+\t[^\t]+\t[^\t]+$/){
        $cptHashSource++;
        my ($token,$tags,$lemma) = StringManager::splitLineTab($line);

        $idToLineSource{$currentId}{$cptToken}{"idSource"}    = $cptHashSource;
        $idTokenSourceToValues{$cptHashSource}{"token"}       = $token;
        $idTokenSourceToValues{$cptHashSource}{"tags"}        = $tags;
        $idTokenSourceToValues{$cptHashSource}{"lemma"}       = $lemma;
        $idTokenSourceToValues{$cptHashSource}{"isFullWord"}  = StringManager::isFullWord(\$lemma, \$tags);
        $idTokenSourceToValues{$cptHashSource}{"isWord"}      = StringManager::isWord(\$lemma, \$tags);

        $cptToken++;
        $tokenToIdLineSource{$token}{$currentId} = 1;
        $idSourceToTokensToCount{$currentId}{$token} = (exists( $idSourceToTokensToCount{$currentId}{$token} )) ? $idSourceToTokensToCount{$currentId}{$token} + 1 : 1;
        $ambLemmaToIdLineSource{$lemma}{$currentId} = 1;
        # if($lemma =~/^.+[|].+$/){
        if(index($lemma,"|") != -1 ){
          my @ambLemmas = split(/\|/,$lemma);
          foreach my $ambLemma (@ambLemmas){
            $lemmaToIdLineSource{$ambLemma}{$currentId} = 1;
            $lemmaCountToIdSource{$currentId}{$ambLemma} = (exists( $lemmaCountToIdSource{$currentId}{$ambLemma} )) ? $lemmaCountToIdSource{$currentId}{$ambLemma} + 1 : 1;
          }
        }
        else{
          $lemmaToIdLineSource{$lemma}{$currentId} = 1;
          $lemmaCountToIdSource{$currentId}{$lemma} = (exists( $lemmaCountToIdSource{$currentId}{$lemma} )) ? $lemmaCountToIdSource{$currentId}{$lemma} + 1 : 1;
        }

        if($tags eq "sent"){
          $cptToken=0;
          my @cptOrdons =  sort ({$a <=> $b} keys (%{$idToLineSource{$currentId}}));
          $currentId++;
        }
    }
  }
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Lecture Source took $run_time seconds\n";
  close SOURCE;
  return;
}

sub makeFuzzyLemmas{
  my ($minLevenstein) = @_;
  #si lemmaCible = a|b et lemmaSource = a|d alors ->
    # $lemmaCibleToFuzzySource{'a'}{'a'} = 1;
    # $lemmaCibleToFuzzySource{'a'}{'d'} = 1;
    # $lemmaCibleToFuzzySource{'b'}{'a'} = 1;
    # $lemmaCibleToFuzzySource{'b'}{'d'} = 1;

  if(! -e($nameFileOutFuzzyLemma)){
      open( FUZZY, ">:encoding(utf8)", $nameFileOutFuzzyLemma );
      my $cpt = 0;
      # my @keysCible = keys %lemmaToIdLineCible;
      # my @keysSource = keys %lemmaToIdLineSource;
      my @keysCible = keys %ambLemmaToIdLineCible;
      my @keysSource = keys %ambLemmaToIdLineSource;

      foreach my $keyCible (@keysCible) {
          # my $progress = $cpt / $#keysCible * 100;
          # print $progress. "% (lemma)\n";
          my @fuzziess;
          my @lemmasCible = split(/\|/,$keyCible);
          my $flagMatch=0;
          foreach my $keySource (@keysSource) {
            foreach my $lemmaCible (@lemmasCible) {
              if(! StringManager::isNotWorthy(\$lemmaCible, \$keySource, \$minLevenstein)){
                  my @lemmasSource=split(/\|/,$keySource);
                  foreach my $lemmaSource (@lemmasSource) {
                    if ( ($lemmaCible eq $lemmaSource) || (StringManager::getDistancePercent($lemmaSource, $lemmaCible) <= $minLevenstein) ) {
                      $flagMatch=1;
                      if(!(grep {$_ eq $lemmaSource} @fuzziess)){
                        push( @fuzziess, $lemmaSource );
                      }
                    }
                  }
                  if($flagMatch){
                    $flagMatch=0;
                    foreach my $lemmaSource (@lemmasSource){
                      if(!(grep {$_ eq $lemmaSource} @fuzziess)){
                        push( @fuzziess, $lemmaSource );
                      }
                    }
                  }
              }
            }
          }

          if($#fuzziess >= 0){
            foreach my $lemmaCible (@lemmasCible) {
              #pour chaque amb de cible (donc a et b), on associe concaténation des autres
              my $tmpAmbs = join('#', @fuzziess);
              print FUZZY $lemmaCible . "=".$tmpAmbs."\n";
            }
          }
          $cpt++;
      }
      close(FUZZY);
      print "FUZZY lemma created.";
    } else {
      print "FUZZY lemma already existing.";
    }
}

sub makeFuzzyTokens {
  my ($minLevenstein) = @_;
  if(! -e($nameFileOutFuzzyToken)){
      open( FUZZY, ">:encoding(utf8)", $nameFileOutFuzzyToken );
      my $cpt = 0;
      my $flagMatch = 0;
      my @keysCible = keys %tokenToIdLineCible;
      my @keysSource = keys %tokenToIdLineSource;
      foreach my $keyCible (@keysCible) {
          my @fuzziess;
          # my $progress = $cpt / $#keysCible * 100;
          # print $progress. "% (token)\n";
          foreach my $keySource (@keysSource) {
            if(! StringManager::isNotWorthy(\$keyCible, \$keySource, \$minLevenstein)){
              if ( ($keyCible eq $keySource) || (StringManager::getDistancePercent($keySource, $keyCible) < $minLevenstein) ) {
                  $tokenCibleToFuzzySource{$keyCible}{$keySource} = 1;
                  $flagMatch = 1;
              }
              if($flagMatch){
                push( @fuzziess, $keySource );
              }
              $flagMatch = 0;
            }
          }
          if($#fuzziess >= 0){
            my $tmpTokens = join('#', @fuzziess);
            print FUZZY $keyCible . "=".$tmpTokens."\n";
          }
          $cpt++;
      }
      close(FUZZY);
      print "FUZZY token created.";
  }
  else {
    print "FUZZY token already existing.";
  }
}

sub readFuzzy{
  my ($lemmaOrToken) = @_;
  my $filename = ($lemmaOrToken eq "lemma") ? $nameFileOutFuzzyLemma: $nameFileOutFuzzyToken;

  if(-e($filename)){
    #lecture du fichier pour mise en variable dans
    open( FUZZY, "<:encoding(utf8)", $filename );
    my @linesFuzzy=<FUZZY>;
    close(FUZZY);
    foreach my $lineFuzzy (@linesFuzzy){
      if($lineFuzzy=~/^([^=]+)=(.+)\s*$/){
        my $keyCible = $1;
        my @keysSource = split("#",$2);
        foreach my $keySource (@keysSource){
          if ($lemmaOrToken eq "lemma") {
            $lemmaCibleToFuzzySource{$keyCible}{$keySource}=1;
          } else {
            $tokenCibleToFuzzySource{$keyCible}{$keySource}=1;
          }
        }
      }
    }
    print "FUZZY lemma/token loaded.";
  }
}
1;
