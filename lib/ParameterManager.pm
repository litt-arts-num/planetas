package ParameterManager;
use strict;
# use warnings;
use utf8;
use locale;

sub getParams {
    my %params;
    print "Lecture des parametres...\n";
    open( PARAMS, "<:encoding(utf8)", "parameters.conf" );
    my $subpattern = "\\s+=\\s+([^\\n\\r]+)";
    while ( ( my $line = <PARAMS> ) ) {
      if ( $line =~ /^pointEmptyWord$subpattern/ ) {
          $params{"pointEmptyWord"} = $1;
          next;
      }
        if ( $line =~ /^slidingWindow$subpattern/ ) {
            $params{"slidingWindow"} = $1;
            next;
        }
        if ( $line =~ /^patterns$subpattern/ ) {
            $params{"patterns"} = $1;
            next;
        }
        if ( $line =~ /^bonusPattern$subpattern/ ) {
            $params{"bonusPattern"} = $1;
            next;
        }
        if ( $line =~ /^bonusSubpattern$subpattern/ ) {
            $params{"bonusSubpattern"} = $1;
            next;
        }
        if ( $line =~ /^displayProgress$subpattern/ ) {
            $params{"displayProgress"} = $1;
            next;
        }
        if ( $line =~ /^windowSize$subpattern/ ) {
            $params{"windowSize"} = $1;
            next;
        }
        if ( $line =~ /^minLevenstein$subpattern/ ) {
            $params{"minLevenstein"} = $1;
            next;
        }
        if ( $line =~ /^distanceQuote$subpattern/ ) {
            $params{"distanceQuote"} = $1;
            next;
        }
        if ( $line =~ /^distanceNE$subpattern/ ) {
            $params{"distanceNE"} = $1;
            next;
        }
        if ( $line =~ /^minRatio$subpattern/ ) {
            $params{"minRatio"} = $1;
            next;
        }
        if ( $line =~ /^bonusEntity$subpattern/ ) {
            $params{"bonusEntity"} = $1;
            next;
        }
        if ( $line =~ /^bonusQuote$subpattern/ ) {
            $params{"bonusQuote"} = $1;
            next;
        }
        if ( $line =~ /^bonusExactMatch$subpattern/ ) {
            $params{"bonusExactMatch"} = $1;
            next;
        }
        if ( $line =~ /^pointTokenEq$subpattern/ ) {
            $params{"pointTokenEq"} = $1;
            next;
        }
        if ( $line =~ /^pointTokenFuzz$subpattern/ ) {
            $params{"pointTokenFuzz"} = $1;
            next;
        }
        if ( $line =~ /^pointLemmaEq$subpattern/ ) {
            $params{"pointLemmaEq"} = $1;
            next;
        }
        if ( $line =~ /^pointLemmaFuzz$subpattern/ ) {
            $params{"pointLemmaFuzz"} = $1;
            next;
        }
        if ( $line =~ /^gapFactor$subpattern/ ) {
            $params{"gapFactor"} = $1;
            next;
        }
        if ( $line =~ /^jokerExtent$subpattern/ ) {
            $params{"jokerExtent"} = $1;
            next;
        }
        if ( $line =~ /^contexteCible$subpattern/ ) {
            $params{"contexteCible"} = $1;
            next;
        }
        if ( $line =~ /^minScore$subpattern/ ) {
            $params{"minScore"} = $1;
            next;
        }
        if ( $line =~ /^maxResults$subpattern/ ) {
            $params{"maxResults"} = $1;
            next;
        }
        if ( $line =~ /^repResults$subpattern/ ) {
            $params{"repResults"} = $1;
            next;
        }
        if ( $line =~ /^sourceFile$subpattern/ ) {
            $params{"sourceFile"} = $1;
            next;
        }
        if ( $line =~ /^repResults$subpattern/ ) {
            $params{"repResults"} = $1;
            next;
        }
        if ( $line =~ /^sourceFile$subpattern/ ) {
            $params{"sourceFile"} = $1;
            next;
        }
    }

    return %params;
}

1;
