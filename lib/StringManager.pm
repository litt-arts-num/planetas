package StringManager;
use strict;
use Text::Trim qw(trim);
# use warnings;
use Text::Levenshtein qw(distance);
use utf8;
use locale;

my $num                 = "<[^\t]+\tadj:num\t[^\t]+>|<[xvi]+\t[^\t]+\t[^\t]+>";
my $liber               = "<[^\t]+\t[^\t]+\tliber>";
my $openQuotesAndColon  = "<[:'\"«“‘‹<]\t[^\t]+\t[^\t]+>";
my $anything            = "<[^\t]+\t[^\t]+\t[^\t]+>";
my $adverbe             = "<[^\t]+\t[^\t]+\t(ut|sic|sicut|apud)>";
my $verbe               = "<[^\t]+\t[^\t]+\t(habeo|dico|lego)>";
my $openQuotes          = "<['\"«“‘‹<]\t[^\t]+\t[^\t]+>";
my $closeQuotes         = "<[»”’›\"'>]\t[^\t]+\t[^\t]+>";
my $poet                = "<[^\t]+\t[^\t]+\t(poeta|ille|lucan[^>]+)>";
my $otherPoets          = "<[^\t]+\t[^\t]+\t(v[ie]rgil[^>]+|horat[^>]+|terenti[^>]+|cicer[^>]+|iuuenal[^>]+|tulli[^>]+)>";

our %masterPatterns;
our %subPatterns;
$masterPatterns{"sic-lucanus"}                = "($adverbe|$poet)($anything){0,5}($openQuotes)";
$subPatterns{"sic-lucanus"}{"relateur"}       = "$adverbe($anything){0,4}($verbe)($anything){0,4}($openQuotes)";
$subPatterns{"sic-lucanus"}{"pouet"}          = "$poet";
$subPatterns{"sic-lucanus"}{"libro"}          = "(($num)($liber)?)|(($liber)?($num))($anything){0,3}($openQuotes)";
$subPatterns{"sic-lucanus"}{"quote-complete"} = "($openQuotes)(<[^\t'\"«“‘‹<]+\t[^\t]+\t[^\t]+>){2,}($closeQuotes)";

our @excludePatterns = ("<[^\t]+\t[^\t]+\tordo>(<[^\t]+\t[^\t]+\tsum\>)?<[^\t]+\t[^\t]+\t:>", "($otherPoets($anything){0,4}$openQuotesAndColon)");

our %stopwords       = ( "in" => 1,"et" => 1,"hic" => 1, "non" => 1, "nunc" => 1);
our %stopPOS         = ( "rel" => 1, "cc" => 1, "prep" => 1, "pron" => 1, "esse:ind" => 1);
our %punctToken      = ( "/" => 1, "»" => 1, "«" => 1, "." => 1);
our %punctPOS        = ( "sent" => 1, "pun" => 1);


sub exactMatch{
  my ($cible, $source, $bonusExactMatch) = @_;
  $cible =~ s/\W//g;
  $source =~ s/\W//g;
  # $stringSource =~ s/ [»”’:;\/\\.!?\(\[\)\]\{\}=\-,›"'>«“‘‹<]//g;
  $cible =~ s/v/u/g;
  $source =~ s/v/u/g;
  if (index($source, $cible) != -1){
     return $bonusExactMatch;
  }

  return 0;
}

sub isWord{
  my ($lemmas, $tags) = @_;
  if((!exists($punctPOS{$$tags})) && (!exists($punctToken{$$lemmas})) ){
    return 1;
  }

  return 0;
}

sub isFullWord{
  my ($lemmas, $tags) = @_;
  if((!exists($stopPOS{$$tags})) && (!exists($stopwords{$$lemmas})) ){
    return 1;
  }

  return 0;
}

sub splitLineTab{
  my ($line) = @_;
  my ($token, $tags, $lemmas) = split("\t",$line);
  $lemmas = trim($lemmas);
  if($lemmas eq "<unknown>" || $lemmas eq "-"){
    $lemmas = $token;
  }

  return ($token,$tags,$lemmas);
}

sub sentenceMatchPattern{
  my ($linesTT, $params) = @_;
  my %params  = %{$params};

  foreach my $excludePattern (@excludePatterns){
    my $evalPattern = qr/$excludePattern/;
    if ($linesTT =~ $evalPattern) {
      return -1;
    }
  }
  my $score   = 0;

  foreach my $keyPattern (keys %masterPatterns){
    my $pattern = $masterPatterns{$keyPattern};
    my $evalPattern = qr/$pattern/;
    if ($linesTT =~ $evalPattern) {
      $score += $params{"bonusPattern"};
      foreach my $keySubPattern (keys %{$subPatterns{$keyPattern}}){
          $pattern = $subPatterns{$keyPattern}{$keySubPattern};
          $evalPattern = qr/$pattern/;
            if ($linesTT =~ $evalPattern) {
            $score += $params{"bonusSubpattern"};
          }
      }
    }
  }

  return $score;
}

sub getDistancePercent{
  my ($strSource, $strCible) = @_;
  my $dist = distance( $strCible, $strSource );
  my $len = ( length($strCible) + length($strSource) ) / 2;
  my $percent = $dist / $len * 100;

  return $percent;
}

sub isNotWorthy{
  my ($strSource, $strCible, $minLevenstein) = @_;
  my $cibleLength = length($$strCible);
  my $sourceLength = length($$strSource);
  if($cibleLength == 0 || $sourceLength == 0){
    return 1;
  }
  elsif($sourceLength > $cibleLength){
    if( ($sourceLength/$cibleLength) > (1+$$minLevenstein/100) ){
      return 1;
    }
  }
  else{
    if( ($cibleLength/$sourceLength) > (1+$$minLevenstein/100) ){
      return 1;
    }
  }
  return 0;
}

1;
